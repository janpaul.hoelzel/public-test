# An Introduction to the ADDup pipeline

This is an interactive script to teach you the basic concepts using Docker, MongoDB and Shiny. As we will tell you most of the stuff in person we will dive directly into the 



## Plot in R

Let us start out by creating a very simple example plot in R with simulated data and without any fancy features. [Here](https://statisticsglobe.com/plot-in-r-example) are some options to choose from (hold _ctrl_ and click). If you have any other data you want to try, do it. Coding is supposed to be fun and creative!

```R
help("barplot")
# defining vector
x <- c(7, 15, 23, 12, 44, 56, 32)
# plotting vector
barplot(x, xlab = "GeeksforGeeks Audience", 
           ylab = "Count", col = "white", 
           col.axis = "darkgreen", 
           col.lab = "darkgreen")
```

Now that we have created a simple plot we can move over to displaying it as an *iframe* in the browser using shiny. 

## Shiny 

Shiny is an R package that is used to create interactive visualizations. Create two new R code scripts. Call one **ui.R** and the other **server.R** and save them to your **app** folder inside **onboarding_pipeline**. Copy the following code into their respective scripts. 

### User Interface

```R
##########
# Script:   name
# Project:  Augmented Deliberative Democracy (ADD-up)
# Author:   your name
# Date:     01.01.2000
##########

ui <- fluidPage(
  # main frame for bar plot
  mainPanel(
   plotOutput("yourplot")
  )
)
```

### Server 

```R
##########
# Script:   name
# Project:  Augmented Deliberative Democracy (ADD-up)
# Author:   your name
# Date:     01.01.2000
# ##########

server <- function(input, output, session) {
  
    # this is where your plot is rendered, the name behind the dollar sign has to be the same as the 
    # one within the "plotOutput("yourplot")" in the ui.R file
  output$yourplot <- renderPlot({
      
      ######## Generate your example data here: #########
      
      ###################################################
      #
      ######## Plot the data here: ######################
      
      ###################################################
      
  })
}

```

If you have successfully created the two scripts and inputted your personal plot and data, search for the green button **Run App** it should be on the top right of you left pane (the code script). Now click **Run App** and a new window should pop up displaying the plot you have previously created. In the top-left you can see two things, first the local port association on you own computer. The number _127.0.0.1_ indicates you local machine, while the second number like _6239_ associates a specific door to the visualization. Imagine these four digit numbers as doors in large building connecting to specific rooms of different utility. Now click to the right on: **Open in Browser** and plot should open in you browser. Do the same by copying you specific URL like: *http://127.0.0.1:6239*. 

I hope this helped to get a basic understanding what a Shiny-App is and how it works. wo now proceed to integrate you app into a container to run it independently. This assumes that you have installed the [Docker Software](https://www.docker.com/products/docker-desktop). If not do it now. 

## Docker Container

Docker container are independent and fully autonomous Linux distributions. We will write a simple example integrating your shiny app into the container

```txt
# 1. Shiny plus Tidyverse Docker (this is the tidyverse version, fixed at :4.0.0)
FROM rocker/shiny-verse:4.0.2

# 2. infos, you can label this script to indicate that you are responsible for the code
LABEL maintainer="Felix Suettmann <felix.suettmann@stud.uni-goettingen.de>"

# 3. install some linux utilities and change some of the files from the rocker version of shiny
RUN apt-get update && apt-get install -y \
    sudo \
	gdebi-core \
    pandoc \
    pandoc-citeproc \
    libcurl4-gnutls-dev \
    libcairo2-dev \
    libxt-dev \
    libssh2-1-dev \
    libssl-dev \
	dos2unix

# 4. install R packages (you can also put your own packages here)
RUN install2.r -e  httr mongolite
	
# 5. install shiny server
RUN wget --no-verbose https://download3.rstudio.org/ubuntu-14.04/x86_64/shiny-server-1.5.14.948-amd64.deb && \
    dpkg -i shiny-server-1.5.14.948-amd64.deb

# 6. Copy shiny-server configuration files into the Docker image
COPY ./shiny-server.sh /usr/bin/shiny-server.sh
COPY ./shiny-server.conf /etc/shiny-server/shiny-server.conf

# 7. Copy shiny app and r scripts into docker
COPY ./app/* /srv/shiny-server/

# 8. Open the port you want to use for the app
EXPOSE 8877

# 9. Some more code to include but nothing you have to understand
###Avoiding running as root --> run container as user instead
# allow permission (https://stackoverflow.com/questions/57421577/how-to-run-r-shiny-app-in-docker-container)
RUN sudo chown -R shiny:shiny /srv/shiny-server
# execute in the following as user --> imortant to give permission before that step
USER shiny

# 10. Start the app
CMD ["/usr/bin/shiny-server.sh"]

```

Lets comment some of the steps from before. 

1. We use a base image with R version 4.0.2 fixed. The name **shiny-verse** also indicates that all packages of the **tidyverse** are already installed in this distribution. This saves us from installing these packages and the pickle of doing a custom installation of R. Imagine it as getting a car that is already fully functional and now we just have to choose if we want the racing interior or the white leather chairs (who doesn't wan those?!). There are also other base images if you are wondering. Might be worth checking [rocker](https://www.rocker-project.org/images/). 
2. We decided to label our images to indicate who is responsible for them, don't feel embarrassed we have all made mistakes
3. updating a lot of relevant libraries
4. If you want to use you own packages for a visualization this is the place to add the packages to be installed in the image. While we are at it why don't you add one that comes to your mind?
5.  to 7. installs Shiny and copies some files from your local machine into the image. 

8. here we open the door to the container imagine it as creating a hallway between the independent container and you local computer
9. to ensure everything works interchangeably between Linux, windows and MacOS we are converting to a specific file format and give some rights.  
10. Here is the start command for the App. We use the **.sh** file, there are other ways but it allows us the specify some more details. Now would be a good idea to open the **.sh** and **.conf** file using a TXT editor to see their contents. Edits  should not be needed.

Find a different introduction [here](https://hosting.analythium.io/running-shiny-server-in-docker/).

### Building the Container-App

Now everything is ready to build our Container. Open the console or PowerShell on your computer. 

Lets see if Docker is installed correctly:

```powershell
docker --version
```

If this does not throw an error run the following command with your path to the folder directory in which all the files are stored (For me this is *C:\Users\felix\Documents\prototype\99_meetings*). 

```powershell
cd Documents\prototype\99_meetings\onboarding_pipeline
```

Next we can build the container. 

```powershell
docker build --rm -t test/onboarding_app -f dockerfile-onboarding .
```

This hopefully proceeds without errors and you can see the different steps being integrated in blue. Downloading and compiling the R-Packages can take a minute or two. Now we can check the image by running, as you can see it is bigger than 2GB. 

```powershell
docker images
```

Let us start the image now. We run:

```powershell
docker run -d -p 8877:8877 test/onboarding_app
```

Notice that we connect the port inside the container to our outside computer. The flag "-d" indicates that the container is supposed to run in the background (meaning the terminal does not get blocked). Try leaving it out if you are interested what happens. 

Can you see your plot is the bigger question. To view it put this into your browser **http://127.0.0.1:8877/yourplot/** or click [here](http://127.0.0.1:8877/yourplot/). Notice that there is an additional tag at the end of the address "yourplot", do not get confused I did that by specifying it in the **.config** file. You might need that knowledge later. 

To stop the App we first have to get the **Container ID**. Run

```powershell
docker ps
```

and copy the ID. Then type 

```powershell
docker stop ID
```

Congrats - you have successfully finished the first part or this tutorial and build a deployable web-app. Take a moment to indulge on this achievement and imagine your future car - maybe with brown leather interior? 



## Second Part -  using the R-Server (still work in progress)

To make developing code within the pipeline easier, we set up an R server that is running within a container and has access to the internal database and streaming. This way we can develop and test new algorithms for the backend data processing more conveniently. We will show you how this all works and provide you with some simple examples but I decided to give some basic commands to interact with the database **MongoDB**. 

### MongoDB 

Some R important commands to work with the MongoDB

```R
library(mongolite)
m = mongo(collection = "all_data", 
          db = "ADDup",
          url = "mongodb://addup:1234password@mongodb:27017")
data <- m$find('{}', field = '{}')
```





